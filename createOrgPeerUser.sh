current_version="v2.3.0"
chainmaker_path="/home/zhao/projects/go/src/chainmaker/chainmaker-go"
cryptogen_path="/home/zhao/projects/go/src/chainmaker/chainmaker-cryptogen"
PORT=${1:-3000}
PORT1=$[$PORT+1]
PORT2=$[$PORT+2]
PORT3=$[$PORT+3]
gen_new_cert(){
# echo "------------在chainmaker-cryptogen下生成org5的节点证书：-------------------"
cd "${cryptogen_path}/bin"
# 1.备份旧的证书文件
rm -rf crypto-config
# 2.复制链上所有节点的证书到bin目录下，适用于在现有组织证书的基础上扩展节点证书
cp -r "${chainmaker_path}/build/crypto-config" ./
# 3. 修改配置文件
# 如果增加一个共识节点，node: -type: consensus 下面的count: 本来是1，改为2就等于增加一个共识节点，适用于在现有组织证书的基础上扩展节点证书
# 如果想增加一个新的组织：修改host_name: wx-org下的count，适用于增加一个新的组织及其节点
# vim ../config/crypto_config_template.yml
# 请手动编辑后保存。
# 4. 执行命令生成新的证书
./chainmaker-cryptogen extend -c ../config/crypto_config_1.yml
}


copy_node_file(){
# echo "------------复制org5节点的包文件-------------------"
cd "${chainmaker_path}/build/release"
#1. 复制org1的节点目录
rm -rf "chainmaker-v2.3.0-wx-org5.chainmaker.org"
cp -r "chainmaker-v2.3.0-wx-org1.chainmaker.org" "chainmaker-v2.3.0-wx-org5.chainmaker.org"
#2. 把chainmaker-*-wx-org5.chainmaker.org/bin下所有的.sh脚本中所有wx-org1.chainmaker.org替换为wx-org5.chainmaker.org"
cd chainmaker-v2.3.0-wx-org5.chainmaker.org/
sed -i 's/org1/org5/g' bin/start.sh
sed -i 's/org1/org5/g' bin/stop.sh
sed -i 's/org1/org5/g' bin/restart.sh
sed -i 's/org1/org5/g' bin/run.sh
#3.删除data和log
rm -rf data/*
rm -rf log/*
# 4. config/下的org1目录重命名为org5
mv config/wx-org1.chainmaker.org config/wx-org5.chainmaker.org
}

update_cert(){
# 使用chainmaker-cryptogen生成的wx-org5.chainmaker.org下的node和user分别覆盖掉wx-org5.chainmaker.org/config/wx-org5.chainmaker.org/certs下的node和user"
crypto_cert=${cryptogen_path}/bin/crypto-config/wx-org5.chainmaker.org
chainmaker_cert=${chainmaker_path}/build/release/chainmaker-v2.3.0-wx-org5.chainmaker.org/config/wx-org5.chainmaker.org/certs
cp -rf $crypto_cert/user $chainmaker_cert
cp -rf $crypto_cert/node $chainmaker_cert
cat $chainmaker_cert/node/consensus1/consensus1.nodeid
}

update_config(){
# echo "----------------修改org5的chainmaker.yml----------------"
# 1. 把wx-org5.chainmaker.org/config/wx-org5.chainmaker.org/chainmaker.yml中所有wx-org1.chainmaker.org替换为wx-org5.chainmaker.org
sed -i 's/org1/org5/g' config/wx-org5.chainmaker.org/chainmaker.yml
# 2. 修改net模块，把 listen_addr: /ip4/0.0.0.0/tcp/11301 修改为 listen_addr: /ip4/0.0.0.0/tcp/11305"
old_config="/ip4/0.0.0.0/tcp/11301"
new_config="/ip4/0.0.0.0/tcp/${PORT}"
sed -i "s%${old_config}\+%${new_config}%g" config/wx-org5.chainmaker.org/chainmaker.yml
sed -i "s/12301/${PORT1}/g" config/wx-org5.chainmaker.org/chainmaker.yml
sed -i "s/14321/${PORT2}/g" config/wx-org5.chainmaker.org/chainmaker.yml
sed -i "s/24321/${PORT3}/g" config/wx-org5.chainmaker.org/chainmaker.yml
# 3. 如果是新增同步节点,将consensus证书改成common证书，如果新增的是共识节点，这一步略过。
# sed -i 's/consensus1/common1/g' config/wx-org5.chainmaker.org/chainmaker.yml
# echo "----------------修改org5的bc1.yml----------------"
# 4. 修改chainmaker-*-wx-org5.chainmaker.org/config/wx-org5.chainmaker.org/chainconfig/bc1.yml中的trust_roots模块
# 把所有 ../config/wx-org1.chainmaker.org 修改为 ../config/wx-org5.chainmaker.org
sed -i "s/wx-org1.chainmaker.org\/certs\/ca/wx-org5.chainmaker.org\/certs\/ca/g" config/wx-org5.chainmaker.org/chainconfig/bc1.yml
}


update_cmc_cert(){
crypto_cert=${cryptogen_path}/bin/crypto-config
cmc_cert=${chainmaker_path}/tools/cmc/testdata/
# ls $crypto_cert
# ls $cmc_cert
cp -rf $crypto_cert $cmc_cert
}

# print() {
# 	echo "hello"
# 	echo $PORT
# 	echo $PORT1
# 	echo $PORT2
# 	echo $PORT3
# }


# print

gen_new_cert
copy_node_file
update_cert
update_config
update_cmc_cert